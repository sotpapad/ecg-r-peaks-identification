% CS370 - Digital Signal Processing
% University of Crete, Computer Science Department
% Sotirios Papadopoulos, Brain and Mind MSC student
% Jan 2020

% ECG Analysis

%% Function that finds the R peaks in a QRS complex
% Input:
%       ecg: signal (mV)
%       time: time step (s)
% Output:
%       t_peaks: time points when an R peak apppers in the ecg signal (s)
%       ampl_R: amplitude of R peaks (mV)
%       heart_rate: self-explanatory!!! (beats/min)

function [t_Rpeaks,ampl_R,heart_rate] = R_peaks_identification(ecg,time)    
    % find the sampling frequency, assuning it always stays the same
    fs = round( 1./(time(2)-time(1)) );
    % assuming a constant sampling rate, find the timestep
    %dt = time(2)-time(1);

    %% 1st step - ideal: filter the signal
    % The first step in an ECG analysis should be to get rid of the noise
    % Two are the main noise sources: the power supply and low frequency
    % electrical activity from the muscles
    
    % create a butterworth bandpass filter for 5 < f < 15 Hz to maximize
    % signal to noise ratio; ignore the low frequency muscles electrical
    % activity
    low = 5;
    high = 15;
    wm = [low high]/(fs/2);             % Nyquist Frequency in denominator
    [num,den] = butter(2,wm);           % 2nd order
    
    % elimination of the group delay that the filter introduces
    filt_ecg = filtfilt(num,den,ecg);
    
    %% 2nd step: find derivative
    % in any case, even in QRS complexes that R is missing (and actually
    % their name doesn't include "R"), the complex always has the maximum
    % derivative; the rise in S-T segmenet is always slower
    
    % on the other hand, T peaks can be higher than R, mainly in subacute-chronic
    % ischaimic myocardial infractions; so, it would be a good idea to look
    % for the peaks of the derivative, rather than the R peaks per se
    der_ecg = zeros(length(filt_ecg),1);
    for i=1:length(filt_ecg)-1
        der_ecg(i+1) = filt_ecg(i+1)-filt_ecg(i);
    end
    % now that we have the derivative of the signal, a good cutoff for the
    % peaks should be mean+2*std
    m = mean(der_ecg);
    s = std(der_ecg);
    cutoff = m + 2*s;
    [~,t_der_peaks] = findpeaks(der_ecg,time,'MinPeakHeight',cutoff);
    
    % if a patient has a pacemaker implanted the derivative at the time
    % points when it "fires" will be very high; so we will exclude these
    % time points in order to the examine only the signal that is
    % intrinsically generated
    for t=1:length(t_der_peaks)
        if t_der_peaks(t) == Inf
            t_der_peaks(t) = 0;
        end
    end
    
    % we should make sure that peaks within 120ms are unique, since
    % that is the minimum time for a QRS complex
    min_qrs_dur = 0.12;
    [ampl_R,t_Rpeaks] = findpeaks(ecg,time,'MinPeakDistance',min_qrs_dur);
    
    % the derivative peaks cannot obviously coincide with the R peaks, but
    % they should be very near; having their time points we can look for
    % peaks in the original ecg signal around these points, say within 10ms
    temp = t_Rpeaks;
    for t=1:length(t_Rpeaks)
        for j=1:length(t_der_peaks)
            diff = t_Rpeaks(t)-t_der_peaks(j);
            if abs(diff) < 0.05
                temp(t) = t_Rpeaks(t);
                break
            else
                temp(t) = 0;
            end
        end
    end
    ampl_R = ampl_R(t_Rpeaks==temp);
    t_Rpeaks = t_Rpeaks(t_Rpeaks==temp);
    
    %% 3rd step: compute heart rate (beats/minute)
    total_dur = max(time)-min(time);
    heart_rate = round((length(t_Rpeaks)/total_dur)*60);
end
