% CS370 - Digital Signal Processing
% Project 4 - Exercise A
% Sotirios Papadopoulos, Brain and Mind MSC student
% Jan 2020

%% First signal
S1 = load('ecg1.txt');
time_1 = S1(:,1);
ecg_1 = S1(:,2);

% analyze the signal
[t_Rpeaks_1,ampl_Rpeaks_1,heart_rate_1] = R_peaks_identification(ecg_1,time_1);
plts(time_1,ecg_1,t_Rpeaks_1,ampl_Rpeaks_1);

%% Second signal
S2 = load('ecg2.txt');
time_2 = S2(:,1);
ecg_2 = S2(:,2);

% analyze the signal
[t_Rpeaks_2,ampl_Rpeaks_2,heart_rate_2] = R_peaks_identification(ecg_2,time_2);
plts(time_2,ecg_2,t_Rpeaks_2,ampl_Rpeaks_2);

%% Function that plots all that is needed for each signal

function [] = plts(time,ecg,t_Rpeaks,ampl_Rpeaks)
    % plot the peaks on the signal
    figure;
    plot(time,ecg,t_Rpeaks,ampl_Rpeaks,'o'); 
    xlabel("Time(s)"); ylabel("Potential (mV)"); title("R peaks identification");

    % find the time intervals between all R peaks
    intervals = zeros(length(t_Rpeaks)-1,1);
    for i=1:length(intervals)
        intervals(i) = t_Rpeaks(i+1) - t_Rpeaks(i);
    end

    % plot the time intervals
    figure;
    stem(intervals);
    xlabel("Intervals"); ylabel("Interval duration (s)");
    title("Time intervals between consequtive R peaks in ECG")

end
